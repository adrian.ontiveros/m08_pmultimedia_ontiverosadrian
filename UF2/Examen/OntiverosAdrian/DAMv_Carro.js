let productos = [
  {
    nombre: "Milkybar",
    foto: "milkybar.jpg",
    descripcion: "Chocolate blanco Nestlé Milkybar 100 g.",
    precio: 1.2,
    descuento: "10%",
  },
  {
    nombre: "Crunch",
    foto: "crunch.jpg",
    descripcion: "Chocolate crujiente Crunch Nestlé sin gluten 100 g.",
    precio: 1.32,
    descuento: "20%",
  },
  {
    nombre: "Croissants - La Bella Easo",
    foto: "croissant.jpg",
    descripcion: "Croissants 0% azucares La Bella Easo 360 g.",
    precio: 2.59,
    descuento: "",
  },
  {
    nombre: "Nesquik",
    foto: "nesquik.jpg",
    descripcion:
      "Chocolate con leche con relleno cremoso Nestlé Nesquik 100 g.",
    precio: 1.31,
    descuento: "30%",
  },
];

let user = localStorage.getItem("clientName");
let carro;

function init() {
  recuperaDatosUsuario();
  muestraProductos();
}

function guardarCliente() {
  let clienteTicket = document.getElementById("cliente");
  let cliente = new Usuario(localStorage.clientName, localStorage.clientSurname, localStorage.clientLocation);
  clienteTicket.innerHTML = cliente.codigoHTML();
}

function recuperaDatosUsuario() {
  /* Función para mostrar el modal de configuración si es la primera vez que se visita la página*/
  if (user === null) {
    // llama al PopUp
    let popUp = window.open(
      "DAMv_PopUp.html",
      "Cookies",
      "width=400,height=350"
    );
  } else {
    guardarCliente();
  }
}

function muestraProductos() {
  /** Función para crear los elementos necesarios para poder visualziar los productos. */
  let productosSection = document.getElementById("productos");

  for (let i = 0; i < productos.length; i++) {
    let producto = document.createElement("article");
    let productoDiv = document.createElement("div");
    let productoNombre = document.createElement("p");
    let productoDescuento = document.createElement("p");
    let productoImg = document.createElement("img");
    let productoPrice = document.createElement("p");
    let productoPriceSpan = document.createElement("span");
    let productoDesc = document.createElement("p");
    let productoPInput = document.createElement("p");
    let productoInput = document.createElement("input");
    let productoButton = document.createElement("button");

    producto.className = "articulo";
    productoNombre.className = "nombre w50";
    productoNombre.innerText = productos[i].nombre;
    productoDescuento.className = "descuento";
    productoDescuento.innerText = productos[i].descuento;
    productoImg.src = "src/" + productos[i].foto;
    productoPriceSpan.innerText = productos[i].precio + "€";
    productoDesc.innerText = productos[i].descripcion;
    productoInput.type = "number";
    productoInput.id = productos[i].nombre.toLowerCase();
    productoInput.step = "1";
    productoInput.min = "0";
    productoButton.addEventListener("click", () => {
        let productName = document.querySelector("#productos .nombre");
      switch(productName.innerHTML){
        case "Milkybar":
            anadirProductos(productos[0]);
            break;
        case "Crunch":
            anadirProductos(productos[1]);
            break;
        case "Croissants - La Bella Easo":
            anadirProductos(productos[2]);
            break;
        case "Nesquik":
            anadirProductos(productos[3]);
            break;
      }
      
    });
    productoButton.innerText = "AÑADIR PRODUCTO";

    productoDiv.appendChild(productoNombre);
    productoDiv.appendChild(productoDescuento);
    productoPrice.appendChild(productoPriceSpan);
    productoPInput.appendChild(productoInput);

    producto.appendChild(productoDiv);
    producto.appendChild(productoImg);
    producto.appendChild(productoPrice);
    producto.appendChild(productoDesc);
    producto.appendChild(productoPInput);
    producto.appendChild(productoButton);

    productosSection.appendChild(producto);
  }
}

function anadirProductos(product) {
  /** Función que muestra el ticket con los productos, precios y descuentos así como el total del precio de la compra. */
  guardarCliente();
  let ticket = document.getElementById("ticket");
  let tableBody = document.getElementById("tableBody");
  ticket.style.display = "flex";

  let descuento = product.descuento.slice(0, -1);
  let cantidad = document.getElementById(product.nombre.toLowerCase());
  console.log(cantidad.value);

  let producto = new Articulo(product.name, product.precio, cantidad.value, descuento);
//   let celdaProducto = creaCelda(producto);
//   tableBody.appendChild(celdaProducto);
}

function creaCelda(valor) {
  let celda = document.createElement("td");
  celda.innerText = valor;
  return celda;
}

function vaciaCarro() {
  let tabla = document.getElementById("tablebody");
  while (tabla.hasChildNodes()) {
    let child = tabla.removeChild(tabla.firstChild);
  }
}
