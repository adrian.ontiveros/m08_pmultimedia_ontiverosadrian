let getDataBtn = document.getElementById("saveCookies");
let clientName = document.getElementById("name");
let clientSurname = document.getElementById("surname");
let clientLoc = document.getElementById("loc");
getDataBtn.addEventListener("click", () => {
  localStorage.setItem("clientName", clientName.value);
  localStorage.setItem("clientSurname", clientSurname.value);
  localStorage.setItem("clientLocation", clientLoc.value);
});
