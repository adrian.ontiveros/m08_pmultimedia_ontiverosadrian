const botonJ1 = document.getElementById('juego1');
const botonJ2 = document.getElementById('juego2');
const botonJ3 = document.getElementById('juego3');
const mensaje = document.getElementById('mensaje');
const puntuacionP = document.getElementById('puntuacion');
const body = document.querySelector('body');
const juego = document.getElementById('juego');
const jugador = document.getElementById('jugador');
let puntuacion = 0;
let tipoEnemigo = 0;

botonJ1.addEventListener('click', () => {
  crearEnemigo();
});
botonJ2.addEventListener('click', () => {
  crearEnemigo2();
  
});
botonJ3.addEventListener('click', () => {
  crearEnemigo3();
});

juego.addEventListener('mousemove', moverJugador);

function moverJugador(evento) {
  jugador.style.top = evento.clientY - juego.offsetTop - jugador.offsetHeight / 2 + 'px';
}


function crearEnemigo() {
  const enemigo = document.createElement('img');
  enemigo.classList.add('enemigo');
  enemigo.style.left = (juego.offsetWidth-200) + 'px';
  enemigo.style.top = Math.random() * (juego.offsetHeight - enemigo.offsetHeight) + 'px';
  enemigo.src = "src/truck.png";
  enemigo.style.width = "150px";
  enemigo.style.height = "60px";
  juego.appendChild(enemigo);
  puntuacion++;
  tipoEnemigo = 1;
  
  moverEnemigo(enemigo, 15, tipoEnemigo);
}

function crearEnemigo2() {
  const enemigo = document.createElement('img');
  enemigo.classList.add('enemigo');
  enemigo.style.left = (juego.offsetWidth-150) + 'px';
  enemigo.style.top = Math.random() * (juego.offsetHeight - enemigo.offsetHeight) + 'px';
  enemigo.src = "src/car2.png";
  enemigo.style.width = "80px";
  enemigo.style.height = "50px";
  juego.appendChild(enemigo);
  puntuacion++;
  tipoEnemigo = 2;

  moverEnemigo(enemigo, 35, tipoEnemigo);
}

function crearEnemigo3() {
  const enemigo = document.createElement('img');
  enemigo.classList.add('enemigo');
  enemigo.style.left = (juego.offsetWidth-100) + 'px';
  enemigo.style.top = Math.random() * (juego.offsetHeight - enemigo.offsetHeight) + 'px';
  enemigo.src = "src/car2.png";
  enemigo.style.width = "130px";
  enemigo.style.height = "50px";
  juego.appendChild(enemigo);
  puntuacion++;
  tipoEnemigo = 3;

  moverEnemigo(enemigo, 50, tipoEnemigo);
}

function moverEnemigo(enemigo, velocidad, tipoEnemigo) {
  let intervalo = setInterval(() => {
    if (enemigo.offsetLeft < 10) {
      clearInterval(intervalo);
      juego.removeChild(enemigo);
    } else if (colision(jugador, enemigo)) {
        clearInterval(intervalo);
        mensaje.style.display = "block";
        body.style.backgroundColor = "#333";
        puntuacionP.innerText = puntuacion;
        jugador.style.top = 140 + 'px';
        while(juego.firstChild){
          juego.removeChild(juego.firstChild);
        }
    } else {
      enemigo.style.left = enemigo.offsetLeft - velocidad + 'px';
    }
  }, 20);

  switch(tipoEnemigo){
    case 1: setTimeout(crearEnemigo, 2000); break;
    case 2: setTimeout(crearEnemigo2, 2000); break;
    case 3: setTimeout(crearEnemigo3, 2000); break;
  }
  
}

function colision(a, b) {
  const aRect = a.getBoundingClientRect();
  const bRect = b.getBoundingClientRect();

  return !(
    (aRect.bottom < bRect.top) || 
    (aRect.top > bRect.bottom) || 
    (aRect.right < bRect.left) || 
    (aRect.left > bRect.right)
  );
}