const button = document.getElementById("cargarIMG");
const spinner = document.getElementById('spinner');
const image = document.getElementById('imgSpinner');

button.addEventListener('click', function () {
    spinner.style.display = 'block'; // Mostrar el spinner cuando se hace clic en el botón

    image.onload = function () {
        spinner.style.display = 'none'; // Ocultar el spinner cuando la imagen se haya cargado
        image.style.display = 'block'; // Mostrar la imagen cuando se haya cargado
    }

    image.src = 'https://picsum.photos/200'; // Cargar la imagen
});