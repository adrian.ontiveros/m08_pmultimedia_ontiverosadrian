function creaCookies() {
    //Creamos la cookie
    document.cookie = "nombre=Alicia";
    document.cookie = "comidaPreferida=Risotto";
}

function visualizaCookies() {
    //Visualizamos
    //console.log(document.cookie); 

    //Todas las cookies estan sepradas por ; por lo que es interesante crear un array para trabajarlas.
    var misCookies = document.cookie;
    var arrayCookies = misCookies.split(";");

    var comida;
    var nombre;
    arrayCookies.map(
        (cookie) => {
            c = cookie.split("=");
            console.log(c);
            if (c[0].includes("comidaPreferida")) {
                comida = c[1];
            }
            if (c[0] == "nombre") {
                nombre = c[1];
            }
        }
    );
    alert("A " + nombre + " le encanta el" + comida);
}

function eliminarCookies() {
    document.cookie.split(";").forEach(function (c) {
        document.cookie = c + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    });

}