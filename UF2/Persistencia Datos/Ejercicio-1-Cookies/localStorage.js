console.log(localStorage);

document.getElementById("addAlumno").addEventListener("click", addAlumno);
document.getElementById("delLocalStorage").addEventListener("click", delLocalStorage);

//Añadimos un nuevo alumno creando un objeto y serializandolo.
function addAlumno(event) {
    const alumno = {
        nombre: document.getElementById('nombre').value,
        apellidos: document.getElementById('apellidos').value,
        curso: document.getElementById('curso').value,
        fechaNacimiento: document.getElementById('fechaNacimiento').value
    };
    //Serialziamos y almacenamos en el localSotorage con clave "alumno+i"
    alumnoSerialized = JSON.stringify(alumno);
    localStorage.setItem('alumno' + localStorage.length, alumnoSerialized);
}

//Eliminanos todos los items del localStorage
function delLocalStorage(event) {
    for (key in localStorage) {
        localStorage.removeItem(key);
    }
}